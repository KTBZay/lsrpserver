# This file is a template, and might need editing before it works on your project.
FROM node:8.11

WORKDIR /DiscordBotCreator/

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /DiscordBotCreator/
RUN npm install

COPY . /DiscordBotCreator/

# replace this with your application's default port
EXPOSE 3210
CMD [ "npm", "start" ]
